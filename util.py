def window(arr, window_size, stride=1):
    for i in range(0, len(arr) - window_size + 1, stride):
        yield arr[i:i + window_size]
