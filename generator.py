import numpy as np


def generate_from_freq_list(freq_l, amplitudes, sample_rate, time, phase=0):
    result = []
    for f, a in zip(freq_l, amplitudes):
        s, p = generate_pitch(sample_rate, f, a, time, phase)
        result.extend(s)
        phase = p
    return np.array(result, np.float32)


def generate_pitch(sample_rate, freq, amplitude, time, phase=0):
    x = np.linspace(0, freq * time * 2 * np.pi, int(sample_rate * time))
    end_phase = ((freq * time) % 1) * 2 * np.pi + phase
    while end_phase > 2 * np.pi:
        end_phase -= 2 * np.pi
    return np.sin(x + phase) * amplitude, end_phase


def triangular_comb_spectrum(n, f):
    retval = np.ndarray(n)
    val = 1
    diff = -1 / (f // 2)
    reverse_after = f // 2
    count = 0
    for i in range(n):
        retval[i] = val
        val += diff
        count += 1
        if count == reverse_after:
            count = 0
            diff = -diff
    return retval


def impulse_comb_spectrum(n, f, limit):
    retval = np.zeros(n)
    c = 0
    for i in range(n):
        if c >= limit:
            return retval
        if i % f == 0:
            retval[i] = 1
            c += 1
    return retval


def sym_impulse_comb_spectrum(n, f, limit):
    retval = np.zeros(n)
    c = 0
    for i in range(n):
        if c >= limit:
            return retval
        if i % f == 0:
            retval[i] = 1
            c += 1
        elif (i + f // 2) % f == 0:
            retval[i] = -1
    return retval
