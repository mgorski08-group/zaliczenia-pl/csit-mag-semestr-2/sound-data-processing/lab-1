import numpy as np
from matplotlib import pyplot as plt

import generator
from util import window


def amplitude(samples):
    return np.average(abs(samples))

def zcr(samples, sampling_rate):
    crossings = 0
    for w in window(samples, 2):
        if (w[0] * w[1]) < 0:
            crossings += 1

    freq = (crossings / 2) * sampling_rate / len(samples)
    return freq


def fft_comb_filter(n, srate):
    spectrum = fix_scale(abs(np.fft.fft(n)))
    fftfreqs = np.fft.fftfreq(len(spectrum), 1/srate)
    spectrum = spectrum[:int(len(spectrum)/2)]
    fftfreqs = fftfreqs[:int(len(fftfreqs)/2)]
    scores = np.zeros(len(spectrum))
    # for f in range(2, len(spectrum)):
    for f in range(2, 30):
        cf = generator.sym_impulse_comb_spectrum(len(spectrum), f, limit=10)
        scores[f] = np.dot(cf, spectrum)
        print(scores[f])
        # plt.plot(fftfreqs, cf)
        # plt.plot(fftfreqs, spectrum)
        # plt.show()
    # plt.plot(fftfreqs, scores)
    # plt.show()
    return fftfreqs[np.argmax(scores)]


def fix_scale(n):
    return n / max(n)
