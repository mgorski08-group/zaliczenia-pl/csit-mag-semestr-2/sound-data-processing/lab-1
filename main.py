import numpy as np
from scipy.io import wavfile
import matplotlib.pyplot as plt

import filters
import generator
import pitch_finding
from util import window


def plot_signal(sampling_rate, signal):
    time_axis = np.arange(len(signal)) / sampling_rate

    plt.figure(figsize=(8, 6))
    plt.plot(time_axis, signal, color="blue", lw=0.5)
    plt.xlabel('Time (seconds)')
    plt.ylabel('Amplitude')
    plt.title('Waveform')
    plt.xlim([0, np.max(time_axis) * 0.3])
    plt.show()


def plot_freq_spectrum(sampling_rate, signal):
    magnitude_spectrum = np.abs(np.fft.fft(signal))
    frequency = np.linspace(0, sampling_rate, len(magnitude_spectrum))

    plt.plot(frequency[:len(frequency) // 2], magnitude_spectrum[:len(magnitude_spectrum) // 2])
    plt.xlabel('Frequency (Hz)')
    plt.ylabel('Magnitude')
    # plt.xlim([0, 3500])
    plt.show()


def zcr_schmitt(samples, sampling_rate):
    crossings = 0
    for w in window(samples, 2):
        if w[0] != w[1]:
            crossings += 1

    freq = (crossings / 2) * sampling_rate / len(samples)
    return freq


def main():
    data = wavfile.read("res/sound_examples/seq/KDF_piano.wav")
    signal = np.asarray(data[1], dtype=np.float64) / 2 ** 15
    sampling_rate = data[0]
    filtered_signal = filters.lowpass_filter(signal, 500, sampling_rate)
    filtered_signal = filters.lowpass_filter(filtered_signal, 500, sampling_rate)
    filtered_signal = filters.lowpass_filter(filtered_signal, 500, sampling_rate)
    filtered_signal = filters.lowpass_filter(filtered_signal, 500, sampling_rate)
    filtered_signal = filters.schmitt_trigger(filtered_signal, 0.02, -0.02)
    freqs = (pitch_finding.zcr(w, sampling_rate) for w in window(filtered_signal, 2048, 2048))
    # freqs = (pitch_finding.fft_comb_filter(w, sampling_rate) for w in window(signal, 2048, 2048))

    # freqs = (filters.quantize_note(f) for f in freqs)

    amplitudes = (pitch_finding.amplitude(w) for w in window(signal, 2048, 2048))

    gen_signal = generator.generate_from_freq_list(freqs, amplitudes, sampling_rate, 2048 / sampling_rate)

    wavfile.write("lowpass4_s0.02.wav", sampling_rate, gen_signal)

    return


if __name__ == '__main__':
    main()
