import numpy as np


def lowpass_filter(signal, cutoff_freq, sampling_rate):
    RC = 1.0 / (2.0 * np.pi * cutoff_freq)
    dt = 1.0 / sampling_rate
    alpha = dt / (RC + dt)

    filtered_signal = np.zeros_like(signal)

    for i in range(1, len(signal)):
        filtered_signal[i] = filtered_signal[i - 1] + alpha * (signal[i] - filtered_signal[i - 1])
    return filtered_signal


def convolve_filter(signal, window_size):
    b = np.ones(window_size) / window_size
    return np.convolve(b, signal, mode="same")


def schmitt_trigger(samples, v_h, v_l):
    output = np.zeros(len(samples))
    state = True

    for i in range(len(samples)):
        if samples[i] > v_h and not state:
            state = True
        elif samples[i] < v_l and state:
            state = False
        output[i] = 1 if state else -1

    return output


def gen_notes():
    retval = [55]
    for i in range(0, 50):
        retval.append(retval[-1] * 2 ** (1 / 12))
    return retval


def quantize_note(freq):
    return min(gen_notes(), key=lambda x: abs(x - freq))
